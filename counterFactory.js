function counterFactory() {
    var incrementDecrement = (function(){
        return {
            increment: function(value){
                return value+1;
            },
            decrement: function(value){
                return value-1;
            }
        };
    })();
    return incrementDecrement;
}
module.exports = counterFactory;