function limitFunctioncallCount(cb ,n){
    const limitFunction =()=>{
        for (let i=0;i< n;i++){
            cb(i);
        }
    }
    return limitFunction();
}
module.exports = limitFunctioncallCount;