const value= 10;

var counterFactory = require("../counterFactory");

var incrementDecrement = counterFactory();

console.log(incrementDecrement.increment(value));
console.log(incrementDecrement.decrement(value));